library ieee;
use ieee.std_logic_1164.all;

entity stepper_motor is
port
(
	triger : out std_logic;
	echo, temp, clk, mode : in std_logic;
	signal phase : out std_logic_vector (3 downto 0)
);
end stepper_motor;

architecture rtl of stepper_motor is
	signal echotime : integer;
	signal sens : std_logic := '1';
	signal vitesse : integer;
begin
	process (clk, echo, temp)
		variable tick,tickecho : integer := 0;
		variable flag : std_logic := '1';
		variable change : std_logic := '0';
	begin
		if (rising_edge(clk) and temp = '0') then
			tick := tick + 1;
			
			if (echo = '1') then 
				tickecho := tickecho + 1;
				flag := '1';
			elsif (flag = '1' and echo = '0') then
				echotime <= tickecho; --cm
				flag := '0';
				tickecho := 0;
			end if;

			case tick is
				when 550 => triger <= '0'; -- 11us
				when 3000000 =>  -- 60ms
					tick := 0;
					triger <= '1';
				when others => null;
			end case;

			if (echotime < 25000) then
				sens <= '1';
				vitesse <= 131072; -- 2^17 --> 16MHz / 2^17 ~ 122Hz
				change := '1';
			elsif (echotime >= 25000 and echotime < 50000 and change = '0') then
				sens <= '0';
				vitesse <= 524288; -- 2^19
			elsif (echotime >= 50000 and echotime < 100000 and not change = '0') then
				sens <= '0';
				vitesse <= 262144; -- 2^18
			elsif (echotime >= 25000 and change = '1') then -- return
				if (echotime >= 1500000) then change := '0'; end if;
			else 
				sens <= '0';
				vitesse <= 131072; -- 2^17
			end if;
		end if;
	end process;
	
	process(clk, sens, vitesse, temp, mode)
		variable gray, clk2 : integer := 0;
	begin
		if (rising_edge(clk) and temp = '0') then
			clk2 := clk2 + 1;
		
			if (clk2 = vitesse) then --2^17 = 131072 pour bonne vitesse
				gray := gray + 1;
				clk2 := 0;
				if ((gray >= 4 and mode = '0') or (gray >= 8 and mode = '1')) then
					gray := 0;
				end if;
			end if;
		
			if (sens = '1') then
				if (mode = '1') then
					case gray is
						-- 8 phases
						when 0 => phase <= "0001";
						when 1 => phase <= "0011";
						when 2 => phase <= "0010";
						when 3 => phase <= "0110";
						when 4 => phase <= "0100";
						when 5 => phase <= "1100";
						when 6 => phase <= "1000";
						when 7 => phase <= "1001";
						when others => phase <= "0000";
					end case;
				else
					case gray is
						-- 4 phases
						when 0 => phase <= "1001";
						when 1 => phase <= "1100";
						when 2 => phase <= "0110";
						when 3 => phase <= "0011";
						when others => phase <= "0000";
					end case;
				end if;
			elsif (sens = '0') then
				if (mode = '1') then
					case gray is
						-- 8 phases
						when 7 => phase <= "0001";
						when 6 => phase <= "0011";
						when 5 => phase <= "0010";
						when 4 => phase <= "0110";
						when 3 => phase <= "0100";
						when 2 => phase <= "1100";
						when 1 => phase <= "1000";
						when 0 => phase <= "1001";
						when others => phase <= "0000";
					end case;
				else
					case gray is
						-- 4 phases
						when 3 => phase <= "1001";
						when 2 => phase <= "1100";
						when 1 => phase <= "0110";
						when 0 => phase <= "0011";
						when others => phase <= "0000";
					end case;
				end if;
			end if;
		end if;
	end process;
end rtl;
