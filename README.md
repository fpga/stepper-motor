## Wiring
<h1 align="center">
	Schematic
</h1>
<p align="center">
<img src=.assets/schematic.png width="100%">
</p>

## Hardware
* [Motor 28BYJ-48 and ULN2003 Driver PCB](https://www.gotronic.fr/art-moteur-28byj-48-08-5-vcc-21213.htm)
* [HC-SR04 Ultrasonic Sensor](https://cpc.farnell.com/multicomp/hc-sr04/ultrasonic-distance-sensor/dp/SN36696)
* [KY-028 Temperature Sensor](https://www.techonicsltd.com/product/ky-028-temperature-sensor-module-digital/)

## Datasheets
* [DE0 Altera FPGA Board](https://www.intel.com/content/dam/altera-www/global/en_US/portal/dsn/42/doc-us-dsnbk-42-5804152209-de0-user-manual.pdf)
* [Motor 28BYJ-48 - Step 4 phases](http://descargas.cetronic.es/28BYJ-48.pdf)
* [Motor Driver PCB ULN2003](https://www.electronicoscaldas.com/datasheet/ULN2003A-PCB.pdf)
* [HC-SR04 Ultrasonic Sensor](https://datasheetspdf.com/pdf-file/1380136/ETC/HC-SR04/1)
* [KY-028 Temperature Sensor (NTC)](https://sensorkit.joy-it.net/en/sensors/ky-028)
